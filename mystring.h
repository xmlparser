#ifndef _MYSTRING_H
#define _MYSTRING_H
// Arbitrary size string struct
// Note: only use with the functions defined herein,
// unless you want to just *READ* from the string
typedef struct mystring {
    char *str;
    int len;
    int maxlen;
    int incr;
} *MString;

// Creates a string
// Note: use mystring_unset() on the return value once you're done with it
// Arguments:
//  initial_size:   initial available size for the string
//  size_increment: by how much the allocated size should grow when needed
// Returns: a pointer to an initialized mystring struct
MString mystring_new(int, int);

// Resets a string
// Note: this only sets the first character to '\0', as well as mystring->cursize to 0
// Arguments:
//  string:         string to be reset
void mystring_clear(MString);

// Enlarges the allocated memory for the string by exactly mystring->incr
// Arguments:
//  string:         string to be enlarged
void mystring_enlarge(MString);

// Appends a string to the mystring struct
// Note: it actually copies toinsert, so you can free it afterwards if appropriate
// Arguments:
//  string:         string to append toinsert to
//  toinsert:       the string to be appended to string
void mystring_str_append(MString, char *);

// Appends a single character to the mystring struct
// Arguments:
//  string:         string to append toinsert to
//  toinsert:       character to append to string
void mystring_char_append(MString, char);

// Frees a mystring struct
// Note: also frees the string held in the struct, so it can't be used anymore either
// Arguments:
//  string:         string to be freed
void mystring_free(MString);

// Gets a line from a file (everything up to a '\n' or EOF) and appends it to a mystring struct
// Note: this may not work on \r\n newlines
// Arguments:
//  string:         string to append to
//  file:           filehandle to read from
int mystring_getline(MString, FILE *);

void mystring_truncate(MString, int);

void mystring_erase(MString, int, int);

int mystring_has_prefix(MString, char *);

int mystring_has_suffix(MString, char *);

int mystring_strip(MString);
#endif
