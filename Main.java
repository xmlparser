import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        XMLTag root = XMLParser.parse(args[0]);
        System.out.println("Right before printing the tree");
        System.in.read();
        root.printTree();
        System.out.println("Right after printing the tree, right before exiting");
        System.in.read();
    }
}
