#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "myarray.h"

MArray myarray_new(int size, int incr, int elsize) {
    MArray myarray = malloc(sizeof(struct myarray));
    myarray->elements = calloc(size, sizeof(void *));
    myarray->len = 0;
    myarray->maxlen = size;
    myarray->incr = incr;
    myarray->elsize = elsize;

    return myarray;
}

static void myarray_enlarge(MArray array) {
    array->maxlen+= array->incr;
    array->elements = realloc(array->elements, array->maxlen*sizeof(void *));
}

void _myarray_insert(MArray array, void *element, int index) {
    void *toinsert = malloc(array->elsize);
    memcpy(toinsert, element, array->elsize);
    if (index < 0 || index > array->len) {
        fprintf(stderr, "Array index out of bounds %s:%d. Index: %d. Array size: %d\n", __FILE__, __LINE__, index, array->len);
        exit(EXIT_FAILURE);
    }
    if (array->len == array->maxlen) {
        myarray_enlarge(array);
    }
    memmove(&array->elements[index+1], &array->elements[index], (array->len - index)*sizeof(void *));
    array->elements[index] = toinsert;
    array->len += 1;
}

void myarray_remove_index(MArray array, int index) {
    free(array->elements[index]);
    memmove(&array->elements[index], &array->elements[index+1], (array->len - index - 1)*sizeof(void *));
    array->len--;
}

void myarray_free(MArray array) {
    int i;
    for (i = 0; i < array->len; i++) {
        free(array->elements[i]);
    }
    free(array->elements);
    free(array);
}

void myarray_reset(MArray array) {
    array->len = 0;
}
