#ifndef _XMLPARSER_H
#define _XMLPARSER_H
#include "myarray.h"
#include "mystring.h"

/// A value representing the boolean \a false.
#define FALSE 0
/// A value representing the boolean \a true.
#define TRUE !FALSE

/**
 * Struct to hold all the information regarding an XML tag.
 */
typedef struct XmlTag {
     /// name of the tag.
    MString             name;
     /// list containing all the children of the tag.
    MArray              children;
     /// value of the tag.
    MString             value;
     /// parent of the tag.
    struct XmlTag       *parent;
} *XmlTag;

/**
 * Creates a new tag.
 * \param parent what will be the parent of the tag. May be NULL.
 * \return a new XmlTag with the specified parent.
 */
XmlTag xml_tag_new(XmlTag parent);

/**
 * Frees the memory associated with the tag.
 * This will free all the memory associated with the tag. So all its members will also be freed.
 * However, none of its children will be freed. To achieve that, use xml_tree_free().
 * \param tag the tag to be freed
 * \see xml_tree_free(XmlTag)
 */
void xml_tag_free(XmlTag tag);

/**
 * Prints a tree of XmlTag's.
 * This will print a tree of [XmlTag]s in a rather strange way, here's an example:
 * \par Input:
 * \verbatim
<test>
    <tagone>
        somevalue
    </tagone>
    <tagtwo />
</test>
\endverbatim
 *
 * \par Output:
 * \verbatim
test; children: 2; 
    tagone; value: somevalue; 
    tagtwo;
\endverbatim
 *
 * \param tag the root XmlTag of the tree to be printed
 * \note \a tag, being regarded as the root of the tree, will not actually be printed.
 */
void xml_tree_print(XmlTag tag);

/**
 * Prints an error message, including where the error occurred.
 * \param message the message to print
 * \param line the line at which the error occurred
 * \param col the column at which the error occurred
 */
void xml_error(const char *message, unsigned int line, int col);

/**
 * Frees an entire tree of XmlTag's.
 * This function works recursively, by calling itself for every child of \a root,
 * right before calling xml_tag_free(XmlTag) with \a root as its argument.
 * \param root the root of the tree to be freed.
 * \see xml_tag_free(XmlTag)
 */
void xml_tree_free(XmlTag root);

/**
 * Parses the file at \a path_to_file.
 * \param path_to_file a string that will be used as the path to the file to be parsed
 * \return the root XmlTag of the parsed file, or NULL if an error occurred.
 */
XmlTag xml_parse(char *path_to_file);

/**
 * This global variable will hold the error status ID as defined in XML_STATUS_ENUM after an error occurs.
 */
int XML_STATUS;

/**
 * List of error IDs.
 */
enum XML_STATUS_ENUM {
    /// No error.
    XML_OK,
    /// A tag has been left unclosed after parsing the entire file.
    XML_UNCLOSED_TAGS,
    /**
     * A value has been encountered without having any open tags.
     * \par Example:
\verbatim
some value <-- this value isn't in any tag.
<tag>
</tag>
some value <-- this value isn't in any tag.
\endverbatim
     */
    XML_VALUE_NO_PARENT,
    /**
     * An invalid character has been encountered. It could be a < when we're defining a tag.
     * \par Example:
     * \verbatim
<tagname<bla> <-- This tag has a <!
\endverbatim
     */
    XML_INVALID_CHAR,
    /** A tag has been closed that wasn't actually the current open tag.
     * \par Example:
     * \verbatim
<one>
<two>
</one> <-- one is not the currently opened tag. two is.
</two>
\endverbatim
     */
    XML_INVALID_CLOSING,
    /// A tag has been opened without a name.
    XML_UNNAMED_TAG,
    /// An I/O error has occurred.
    XML_FILE_ERROR
};
#endif
