import java.util.*;

public class XMLTag {
    StringBuilder name;
    StringBuilder value;
    ArrayList<XMLTag> children;
    XMLTag parent;
    static int depth = 0;

    public XMLTag(XMLTag parent) {
        this.parent = parent;
        children = new ArrayList<XMLTag>();
        name = new StringBuilder();
        value = new StringBuilder();
    }

    public String getName() {
        return name.toString();
    }

    public String getValue() {
        return value.toString();
    }

    public void setName(String name) {
        this.name = new StringBuilder(name);
    }

    public void setValue(String value) {
        this.value = new StringBuilder(value);
    }

    public void addNameChar(char c) {
        name.append(c);
    }

    public void addValueChar(char c) {
        value.append(c);
    }

    public void addChild(XMLTag child) {
        children.add(child);
    }

    public void removeChild(XMLTag child) {
        children.remove(child);
    }

    public ArrayList<XMLTag> getChildren() {
        return children;
    }

    public XMLTag getParent() {
        return parent;
    }

    public boolean check() {
        if (name.length() == 0) return false;
        return true;
    }

    public void printTree() {
        for (XMLTag tag: getChildren()) {
            tag.printTreeHelper();
        }
    }

    private static String multiplyChar(char c, int length) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < length; i++) {
            b.append(c);
        }
        return b.toString();
    }

    private void printTreeHelper() {
        System.out.printf("%s%s; ", multiplyChar(' ', 4*depth), getName());
        if (getChildren().size() > 0) {
            System.out.printf("children: %d; ", getChildren().size());
        }
        if (getValue().length() > 0) {
            System.out.printf("value: %s;", getValue());
        }
        System.out.println();
        depth++;
        for (XMLTag tag: getChildren()) {
            tag.printTreeHelper();
        }
        depth--;
    }
}
