import java.util.*;
import java.io.*;

public class XMLParser {
    public static XMLTag parse(String path) {
        BufferedReader input;
        try {
            input = new BufferedReader(new FileReader(path));
        } catch (IOException e) {
            System.out.println("Error opening file");
            return null;
        }
        String buffer;
        XMLTag head = new XMLTag(null);
        XMLTag currenttag = head;
        boolean opentag = false;
        boolean escape = false;
        char c;

        while (true) { try {
                buffer = input.readLine();
            } catch (IOException e) {
                break;
            }
            if (buffer == null) {
                break;
            }

            for (int i = 0; i < buffer.length(); i++) {
                c = buffer.charAt(i);
                switch (c) {
                    case '\\':
                        if (escape) escape = false;
                        else {
                            escape = true;
                            break;
                        }
                    case '<':
                        if (!escape) {
                            if (opentag) {
                                try {
                                    input.close();
                                } catch (Exception e) {}
                                System.out.println("1");
                                return null;
                            }
                            opentag = true;
                            currenttag = new XMLTag(currenttag);
                            currenttag.getParent().getChildren().add(currenttag);
                            break;
                        }
                    case '>':
                        if (opentag) opentag = false;
                        else {
                            try {
                                input.close();
                            } catch (Exception e) {}
                            System.out.println("2");
                            return null;
                        }
                        if (currenttag.getName().startsWith("/")) {
                            if (!currenttag.getName().substring(1).equals(currenttag.getParent().getName())) {
                                try {
                                    input.close();
                                } catch (Exception e) {}
                                System.out.println("3");
                                return null;
                            }
                            currenttag.getParent().getChildren().remove(currenttag);
                            currenttag = currenttag.getParent().getParent();
                        } else if (currenttag.getName().endsWith(" /")) {
                            currenttag.setName(currenttag.getName().substring(0,currenttag.getName().length() - 2));
                            if (!currenttag.check()) {
                                try {
                                    input.close();
                                } catch (Exception e) {}
                                System.out.println("3");
                                return null;
                            }
                            currenttag = currenttag.getParent();
                        } else {
                            if (!currenttag.check()) {
                                try {
                                    input.close();
                                } catch (Exception e) {}
                                System.out.println("4");
                                return null;
                            }
                        }
                        break;
                    default:
                        if (currenttag == head) {
                            try {
                                input.close();
                            } catch (Exception e) {}
                            System.out.println("5");
                            return null;
                        }
                        if (opentag) {
                            currenttag.addNameChar(c);
                        } else {
                            currenttag.addValueChar(c);
                        }
                }
            }
        }

        try {
            input.close();
        } catch (Exception e) {}

        if (currenttag != head) {
            System.out.println("6");
            return null;
        }

        return currenttag;
    }
}
