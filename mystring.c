#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mystring.h"

MString mystring_new(int size, int incr) {
    MString string = calloc(1, sizeof(struct mystring));
    string->str = malloc((size+1)*sizeof(char));
    string->str[0] = '\0';
    string->len = 0;
    string->maxlen = size;
    string->incr = incr;

    return string;
}

void mystring_clear(MString string) {
    string->len = 0;
    string->str[0] = '\0';
}

void mystring_enlarge(MString string) {
    string->maxlen += string->incr;
    string->str = realloc(string->str, (string->maxlen + 1) * sizeof(char));
}

void mystring_char_append(MString string, char toinsert) {
    if (string->len == string->maxlen) {
        mystring_enlarge(string);
    }
    string->str[string->len] = toinsert;
    string->str[string->len+1] = '\0';
    string->len++;
}

void mystring_str_append(MString string, char *toinsert) {
    int i;
    for (i = 0; toinsert[i] != '\0'; i++) {
        mystring_char_append(string, toinsert[i]);
    }
}

void mystring_free(MString string) {
    free(string->str);
    free(string);
}

int mystring_getline(MString string, FILE *file) {
    int c;
    int count = 0;
    while ((c = fgetc(file)) != EOF && c != '\n') {
        mystring_char_append(string, c);
        count++;
    }
    if (count == 0 && c == EOF) {
        return -1;
    }
    return count;
}

void mystring_truncate(MString string, int index) {
    string->str[index] = '\0';
    string->len -= string->len - index;
}

void mystring_erase(MString string, int index, int len) {
    memmove(&string->str[index], &string->str[index+len], string->len-len);
    string->len -= len;
    string->str[string->len] = '\0';
}

int mystring_has_prefix(MString string, char *substr) {
    return !strncmp(string->str, substr, strlen(substr));
}

int mystring_has_suffix(MString string, char *substr) {
    return !strcmp(string->str + string->len - strlen(substr), substr);
}

int mystring_strip(MString string) {
    static int i = 0;
    if (string == NULL) {
        return i;
    }

    i = 0;
    for (i = string->len - 1; i >= 0; i--){
        if (!isspace(string->str[i])) {
            break;
        }
    }
    if (i+1 < string->len) mystring_truncate(string, i+1);

    for (i = 0; i < string->len; i++) {
        if (!isspace(string->str[i])) {
            break;
        }
    }

    mystring_erase(string, 0, i);
    return i;
}
