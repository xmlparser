#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "xmlparser.h"
#include "myarray.h"
#include "mystring.h"

const char *XML_E_VALUE_NO_PARENT     =   "Value without a parent!";
const char *XML_E_INVALID_CHAR        =   "Invalid character in current parsing status!";
const char *XML_E_INVALID_CLOSING     =   "Trying to close a tag that is not the current one!";
const char *XML_E_UNNAMED_TAG         =   "Trying to open a tag that has no name!";
const char *XML_E_UNCLOSED_TAGS       =   "Not all tags have been closed!";

// Creates an empty XmlTag
// Arguments:
// * parent: the parent of the tag being created (may be NULL)
// Returns:
// An XmlTag, with all the fields ready for usage
XmlTag xml_tag_new(XmlTag parent) {
    XmlTag tag = malloc(sizeof(struct XmlTag));
    tag->name = mystring_new(1,1);
    tag->children = myarray_new(1, 1, sizeof(XmlTag));
    tag->parent = parent;
    tag->value = mystring_new(1,1);

    return tag;
}

// Frees an XmlTag and all its fields
// Arguments:
// * tag: the tag to free
void xml_tag_free(XmlTag tag) {
    mystring_free(tag->name);
    myarray_free(tag->children);
    mystring_free(tag->value);
    free(tag);
}

// Frees an entire tree of XmlTags
// Arguments:
// * root: the root of the tree you wish to free
void xml_tree_free(XmlTag root) {
    int i;
    for (i = 0; i < root->children->len; i++) {
        xml_tree_free(myarray_get(root->children, XmlTag, i));
    }
    xml_tag_free(root);
}

void xml_print_tree_helper(XmlTag tag) {
    static int depth = 0;
    int i;
    printf("%*s%s; ", depth*4, "\0", tag->name->str);
    if (tag->children->len > 0) {
        printf("children: %d; ", tag->children->len);
    }
    if (tag->value->len > 0) {
        printf("value: %s; ", tag->value->str);
    }
    printf("\n");
    depth++;
    for (i = 0; i < tag->children->len; i++) {
        xml_print_tree_helper(myarray_get(tag->children, XmlTag, i));
    }
    depth--;
}

// Prints the tag tree
// Arguments:
// * tag: This should be the root of the tree, and it won't be printed.
void xml_tree_print(XmlTag tag) {
    int i;
    for (i = 0; i < tag->children->len; i++) {
        xml_print_tree_helper(myarray_get(tag->children, XmlTag, i));
    }
}

// Prints an error
// Arguments:
// * message: the error message to print
// * line: the line at which the error occurred
// * col: the column at which the error occurred
void xml_error(const char *message, unsigned int line, int col) {
    printf("ERROR (%u,%d): %s\n", line, col + mystring_strip(NULL) + 1, message);
}

// Checks a tag for validity
// Arguments:
// * tag: the tag to check
// * line the line at which the tag was defined
// * the column at which the tag was defined
// Returns:
// TRUE if the tag has a nonzero length name
// FALSE if the tag has a zero length name
int xml_tag_check(XmlTag tag, int line, int col) {
    if (tag->name->len == 0) {
        XML_STATUS = XML_UNNAMED_TAG;
        xml_error(XML_E_UNNAMED_TAG, line, col);
        return FALSE;
    }
    return TRUE;
}

// This will parse the tagname to extract attributes from it
void xml_tag_parse_attributes(XmlTag tag) {
}

// Parses an XML file
// Arguments:
// * path_to_file: path to the XML file to parse
// Returns:
// The root of the XML tree, or NULL if an error occurred.
// In the latter case, XML_STATUS will be set according to the
// error.
XmlTag xml_parse(char *path_to_file) {
    FILE *input = fopen(path_to_file, "r");
    if (input == NULL) {
        XML_STATUS = XML_FILE_ERROR;
        return NULL;
    }
    MString buffer = mystring_new(1,1);

    // The tag we're currently processing children/values for
    XmlTag head = xml_tag_new(NULL);
    mystring_str_append(head->name, "_ROOT_");
    XmlTag currenttag = head;
    // Are we parsing a tag definition?
    unsigned short opentag = FALSE;
    // Is the current character escaped?
    unsigned short escape = FALSE;
    
    int i;
    unsigned int line = 1;
    char c;
    while (mystring_getline(buffer, input) != -1) {
        mystring_strip(buffer);
        for (i = 0; i < buffer->len; i++) {
            c = buffer->str[i];
            switch (c) {
                case '\\':
                    if (escape) escape = FALSE;
                    else {
                        escape = TRUE;
                        break;
                    }
                case '<':
                    if (!escape) {
                        if (opentag) {
                            XML_STATUS = XML_INVALID_CHAR;
                            xml_error(XML_E_INVALID_CHAR, line, i);
                            mystring_free(buffer);
                            fclose(input);
                            xml_tree_free(head);
                            return NULL;
                        }
                        opentag = TRUE;
                        currenttag = xml_tag_new(currenttag);
                        myarray_append(currenttag->parent->children, currenttag);
                        break;
                    }
                case '>':
                    if (opentag) opentag = FALSE;
                    else {
                        XML_STATUS = XML_INVALID_CLOSING;
                        xml_error(XML_E_INVALID_CLOSING, line, i);
                        mystring_free(buffer);
                        fclose(input);
                        xml_tree_free(head);
                        return NULL;
                    }
                    if (mystring_has_prefix(currenttag->name, "/")) {
                        if (strcmp(currenttag->name->str + 1, currenttag->parent->name->str)) {
                            printf("%s != %s\n", currenttag->name->str + 1, currenttag->parent->name->str);
                            XML_STATUS = XML_INVALID_CLOSING;
                            xml_error(XML_E_INVALID_CLOSING, line, i-currenttag->name->len - 1);
                            mystring_free(buffer);
                            fclose(input);
                            xml_tree_free(head);
                            return NULL;
                        }
                        myarray_remove_index(currenttag->parent->children, currenttag->parent->children->len - 1);
                        XmlTag temp = currenttag;
                        currenttag = temp->parent->parent;
                        xml_tag_free(temp);
                    } else if (mystring_has_suffix(currenttag->name, " /")) {
                        mystring_truncate(currenttag->name, currenttag->name->len - 2);
                        if (!xml_tag_check(currenttag, line, i)) {
                            mystring_free(buffer);
                            fclose(input);
                            xml_tree_free(head);
                            return NULL;
                        }
                        currenttag = currenttag->parent;
                    } else {
                        if (!xml_tag_check(currenttag, line, i)) {
                            mystring_free(buffer);
                            fclose(input);
                            xml_tree_free(head);
                            return NULL;
                        }
                    }
                    break;
                default:
                    if (currenttag == head) {
                        XML_STATUS = XML_VALUE_NO_PARENT;
                        xml_error(XML_E_VALUE_NO_PARENT, line, i);
                        printf("%s\n", buffer->str);
                        mystring_free(buffer);
                        fclose(input);
                        xml_tree_free(head);
                        return NULL;
                    }
                    if (opentag) {
                        mystring_char_append(currenttag->name, c);
                    } else {
                        mystring_char_append(currenttag->value, c);
                    }
            }
        }
        line++;
        mystring_clear(buffer);
    }

    if (currenttag != head) {
        XML_STATUS = XML_UNCLOSED_TAGS;
        xml_error(XML_E_UNCLOSED_TAGS, 0, 0);
        mystring_free(buffer);
        fclose(input);
        xml_tree_free(head);
        return NULL;
    }

    fclose(input);
    mystring_free(buffer);
    return head;
}
