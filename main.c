#include <stdio.h>
#include <stdlib.h>
#include "xmlparser.h"
#include "myarray.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("I need _one_, exactly _one_ XML file as input!\n");
        exit(EXIT_FAILURE);
    }

    printf("Pause...(right before parsing)\n");
    getchar();
    XmlTag tag = xml_parse(argv[1]);
    if (tag == NULL) {
        printf("An error occurred during parsing. Exiting now...\n");
        exit(EXIT_FAILURE);
    }

    printf("Pause...(right after parsing, and right before printing the tree)\n");
    getchar();
    xml_tree_print(tag);
    printf("Pause...(right after parsing, and right before freeing the tree)\n");
    getchar();
    xml_tree_free(tag);
    printf("Pause...(right after freeing the tree)\n");
    getchar();

    return 0;
}
