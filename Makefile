CFLAGS += -Wall -O3

.PHONY: clean

all: main

main: main.c xmlparser.h xmlparser.o myarray.o mystring.o
	${CC} -o xmlparser main.c xmlparser.o myarray.o mystring.o ${CFLAGS}

xmlparser.o: xmlparser.c xmlparser.h
	${CC} -c xmlparser.c ${CFLAGS}

myarray.o: myarray.c myarray.h
	${CC} -c myarray.c ${CFLAGS}

mystring.o: mystring.c mystring.h
	${CC} -c mystring.c ${CFLAGS}

doc: xmlparser.h xmlparser.c Doxyfile
	doxygen

clean:
	rm -r xmlparser xmlparser.o myarray.o mystring.o doc/
